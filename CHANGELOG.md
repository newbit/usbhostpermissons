**v1.0.2**
- upgraded to newer Magisk Module Structure
- added automated release script

**v1.0.1**
- migrated to gitlab

**v1.0.0**
- initial release