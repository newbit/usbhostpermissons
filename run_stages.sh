#!/bin/bash

setup_vars() {
	set -a # Export variables
    : "${PERMALINK:=${CI_SERVER_URL}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}/-/releases/permalink/latest/downloads/${CI_PROJECT_NAME}}"
    set +a # Stop
    update_versions
    set -a # Export variables
    : "${OUT_DIR:=out}"
    : "${MODULE_VER:=$(sed -n 's/^version=//p' module.prop)}"
    : "${MAGISK_MODULE_ID:=$(sed -n 's/^id=//p' module.prop)}"
	: "${MAGISK_MODULE_ZIP:=$MAGISK_MODULE_ID-$MODULE_VER.zip}"
	: "${PACKAGE_REGISTRY_URL:=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${MAGISK_MODULE_ID}/${CI_COMMIT_TAG}}"
    set +a # Stop
}

running_stages() {
    setup_vars

    if [ -z "$@" ]; then
        echo "Please use --before_script or --script or --after_script!"
        exit 1
    fi

    for arg in "$@"; do
        case "${arg}" in
            --before_script | -b)
                echo "Running ${arg#*--}"
                apt_update_install
                get_release_cli
            ;;
            --script | -u)
                echo "Running ${arg#*--}"
                echo "Running Stage build"
                build
                echo "Running Stage upload"
	            upload
                echo "Running Stage release"
                release
            ;;
            --after_script | -a)
                echo "Running ${arg#*--}"
                if $PUSHBACK; then
                    echo "Cloning the repo"
                    git_clone_project
                    echo "Pushing Changes back to Gitlab"
                    git_commit_push_back
                else
                    echo "No pushing back!"
                fi
                echo "Cleaning Up Pipelines"
                cleanup_pipelines
            ;;
        esac
    done    
}

cleanup_pipelines() {
    local PER_PAGE=100
    local PAGE=1
    while JOBS=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "${CI_API_V4_URL}/projects/$CI_PROJECT_ID/pipelines?per_page=$PER_PAGE&page=$PAGE&sort=asc") && [ "$JOBS" != "[]" ];do
        for ID in $(echo $JOBS | jq .[].id);do
            if [ $ID -ne $CI_PIPELINE_ID ]; then
                curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --request "DELETE" "${CI_API_V4_URL}/projects/$CI_PROJECT_ID/pipelines/$ID"
                echo "deleting $ID"
            fi
        done
        PAGE=$((PAGE+1))
    done
}

build() {
    mkdir -p "$OUT_DIR/"
	echo "Building Magisk Module Zip"
    mkdir -p "$OUT_DIR/"
    rm -Rf .git
    zip -r -X "$OUT_DIR/$MAGISK_MODULE_ZIP" * .git* -x '*.DS_Store' '*out*'
}

apt_update_install() {
    apt update && apt install -y zip curl git jq
}
get_release_cli() {
    curl --location --output /usr/local/bin/release-cli "https://gitlab.com/api/v4/projects/gitlab-org%2Frelease-cli/packages/generic/release-cli/latest/release-cli-linux-amd64"
    chmod +x /usr/local/bin/release-cli
    echo "$(release-cli -v)"
}

git_clone_project() {
    git clone "https://${GITLAB_USER_NAME}:${GITLAB_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git" "${CI_COMMIT_SHA}"
    git config --global user.email "${GITLAB_USER_EMAIL}"
    git config --global user.name "${GITLAB_USER_NAME}"
}

git_commit_push_back() {
    local file="update.json"

    echo "Copying changed files to .git dir"
    cp -af "${file}" "${CI_COMMIT_SHA}/"
    cd "${CI_COMMIT_SHA}"
        git add "${file}"
    cd -

    file="module.prop"
    cp -af "${file}" "${CI_COMMIT_SHA}/"
    cd "${CI_COMMIT_SHA}"
        git add "${file}"
        git commit -m "Update to ${CI_COMMIT_TAG}"
        echo "Pushing back updated changes to Repo"
        git push origin "${CI_DEFAULT_BRANCH}" -o ci.skip
    cd -
}

release() {
	release-cli create --name "$CI_COMMIT_TAG" --description 'CHANGELOG.md' --tag-name $CI_COMMIT_TAG \
    --assets-link "{\"name\":\"${MAGISK_MODULE_ZIP}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${MAGISK_MODULE_ZIP}\",\"link_type\":\"other\",\"filepath\":\"/${MAGISK_MODULE_ID}\"}"
    echo "Permalink to the latest:"
    echo "${PERMALINK}"
}

upload() {
    curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "${OUT_DIR}/${MAGISK_MODULE_ZIP}" "${PACKAGE_REGISTRY_URL}/${MAGISK_MODULE_ZIP}"
}

convert_version_2Code() {
    local version="$1"
    local ersion=${version#*v}
    local version_number=( ${ersion//./ } )
	printf -v VERSION_CODE '%1d%02d%02d' ${version_number[0]#*v} ${version_number[1]} ${version_number[2]}
	export VERSION_CODE
}

update_versions() {
    local file="update.json"
	convert_version_2Code "$CI_COMMIT_TAG"

	echo "updating ${file}"
    sed -i -e '/\"version\"/c\    \"version\": \"'"$CI_COMMIT_TAG"'\",' "${file}"
	sed -i -e '/\"versionCode\"/c\    \"versionCode\": '"$VERSION_CODE"',' "${file}"
    sed -i -e '/\"zipUrl\"/c\    \"zipUrl\": \"'"$PERMALINK"'\",' "${file}"

	file="module.prop"
	echo "updating ${file}"
	sed -i -e '/version=/c\version='"$CI_COMMIT_TAG" "${file}"
	sed -i -e '/versionCode=/c\versionCode='"$VERSION_CODE" "${file}"
}

running_stages "$@"
